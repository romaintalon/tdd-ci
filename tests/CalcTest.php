<?php

require __DIR__ . '/../src/Calc.php';

use PHPUnit\Framework\TestCase;

class CalcTest extends TestCase
{
	public function testSomme() {
		$tests = [
			[ 1, 2, 3],
			[-1, -2, -3],
			[-1, 1, 0],
		];
		foreach ($tests as $t) {
			$this->assertEquals($t[2], Calc::somme($t[0], $t[1]));
		}
	}

	public function testProduit() {
		$tests = [
			[ 1, 2, 2],
			[ -1, 1, -1],
			[ -1, 0, 0],
		];
		foreach ($tests as $t) {
			$this->assertEquals($t[2], Calc::produit($t[0], $t[1]));
		}
	}
}
