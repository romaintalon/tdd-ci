<?php

require_once __DIR__ . '/Calc.php';

if (array_key_exists("submit", $_POST)) {
	extract ($_POST);
	$result = Calc::$operation($valeur1, $valeur2);
} else {
	$valeur1 = $valeur2 = $operation = $result = null;
}
?>

<html>
<body>
	<form method="post">
		<input id="valeur1" name="valeur1" value="<?php echo $valeur1?>"/>
		<input id="valeur2" name="valeur2" value="<?php echo $valeur2?>"/>
		<select id="operation" name="operation">
			<option value="somme" <?php if ($operation == 'somme') echo 'selected';?>>+</option>
			<option value="produit" <?php if ($operation == 'produit') echo 'selected';?>>x</option>
		</select>
		<input type="submit" id="submit" name="submit" value="Submit"/>
	</form>

	<div id="result"><?php echo $result?></div>
</body>
</html>
